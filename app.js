//Adding Express
const express = require("express");
const app = express();

//Using ejs for file rendering
app.set("view engine", "ejs");
app.use(express.static("public"));

//Added body-parser
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: true}));

//Sanitizer installation for allowing non harmful html input by the user
const expressSanitizer = require("express-sanitizer");
app.use(expressSanitizer());

//Using method override to turn Post requests into put and delete
const methodOverride = require("method-override");
app.use(methodOverride("_method"));

//Mongoose instalation for mongoDB
const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/restful_blog_app', { useNewUrlParser: true, useUnifiedTopology: true }); 

//Schema and model
const blogSchema = new mongoose.Schema({
        title: String,
        image: String,
        body: String,
        created: {type: Date , default: Date.now}
});

const Blog =mongoose.model("Blog", blogSchema);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~RESTFUL ROUTES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//1. INDEX ROUTE

app.get("/",function(req, res){
    res.redirect("/blogs");
});

app.get("/blogs",function(req, res){
    Blog.find({},function(err, blogs){
        if(err){
            console.log("Error " + err);
        }
        else{
            res.render("index", {blogs: blogs});
        }
    });
});

//2. NEW ROUTE

app.get("/blogs/new", function(req, res){
    res.render("new");
});

//3. CREATE ROUTE 

app.post("/blogs",function(req, res){
    req.body.blog.body =req.sanitize(req.body.blog.body);
    const data =req.body.blog;
    Blog.create(data, function(err, newBlog){
        if(err){
            res.render("new");
        }
        else{
            res.redirect("/blogs")
        }
    });
});

//4. SHOW ROUTE

app.get("/blogs/:id",function(req,res){
    Blog.findById(req.params.id, function(err, foundBlog){
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.render("show", {blog: foundBlog});
        }
    });
});

//5. EDIT ROUTE

app.get("/blogs/:id/edit",function(req,res){
    Blog.findById(req.params.id,function(err,foundBlog){
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.render("edit",{blog: foundBlog});
        }
    });
});

//6. UPDATE ROUTE

app.put("/blogs/:id",function(req, res){
    Blog.findByIdAndUpdate(req.params.id, req.body.blog, function(err, updatedBlog){
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.redirect("/blogs/" + req.params.id);
        }
    });
});

//7. DELETE ROUTE

app.delete("/blogs/:id", function(req,res){
    Blog.findByIdAndRemove(req.params.id,function(err){
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.redirect("/blogs");
        }
    })
})

//Server connection on Port 3000
app.listen(3000,function(){
    console.log("Listening");
})